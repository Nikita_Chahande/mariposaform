import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';



@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  formGroup: FormGroup;
  titleAlert: string = 'This field is required';

  constructor(private formBuilder: FormBuilder) { 
    this.formGroup = formBuilder.group({
      email: formBuilder.control(null, Validators.required),
      password: formBuilder.control(null, Validators.required),
      validate: formBuilder.control(null, Validators.required)
  });
    
  }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    let emailregex: RegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    
    this.formGroup = this.formBuilder.group({
      'email': [null, [Validators.required, Validators.pattern(emailregex)]],
      'password': [null, [Validators.required, this.checkPassword]],
      'validate': ''
    });
  }



  checkPassword(control: { value: any; }) {
    let enteredPassword = control.value
    let passwordCheck = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/;
    return (!passwordCheck.test(enteredPassword) && enteredPassword) ? { 'requirements': true } : null;
  }


  getErrorEmail() {
    return this.formGroup.controls['email'].hasError('required') ? 'Field is required' :
      this.formGroup.controls['email'].hasError('pattern') ? 'Not a valid emailaddress' :
        this.formGroup.controls['email'].hasError('alreadyInUse') ? 'This emailaddress is already in use' : '';
  }
  
  getErrorPassword() {
    return this.formGroup.controls['password'].hasError('required') ? 'Field is required (at least eight characters, one uppercase letter and one number)' :
      this.formGroup.controls['password'].hasError('requirements') ? 'Password needs to be at least eight characters, one uppercase letter and one number' : '';
  }


}